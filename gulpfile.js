var gulp = require('gulp');
var sass = require('gulp-sass');
 
sass.compiler = require('node-sass');

var uglifycss = require('gulp-uglifycss');
 
//this is compilation task for scss we have here 

gulp.task('sass', function () {
  return gulp.src('./*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./css'));
});



//this is for minified css and css is the task name for this 

gulp.task('css', function () {
  return gulp.src('./css/*.css')
    .pipe(uglifycss({
      "maxLineLen": 80,
      "uglyComments": true
    }))
    .pipe(gulp.dest('./dist/'));
});


gulp.task('run' , gulp.series('sass' , 'css'));

//this is task to watch if any file is chaneg as in watch list .... 
gulp.task('watch' , function(){
	gulp.watch('./*.scss' , gulp.series('sass'));
	gulp.watch('./css/*.css' , gulp.series('css'));
});

gulp.task('default' , gulp.series('run' , 'watch'));